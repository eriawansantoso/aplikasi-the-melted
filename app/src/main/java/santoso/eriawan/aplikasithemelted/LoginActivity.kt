package santoso.eriawan.aplikasithemelted

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_pengunjung.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btnLgn -> {
                var email = edUserName.text.toString()
                var password = edPassword.text.toString()

                if(email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(this, "Username / Password tidak boleh kosong", Toast.LENGTH_LONG).show()
                }else {
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating.....")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener {
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            Toast.makeText(this, "Successfully Login", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this, ProdukActivity::class.java)
                            startActivity(intent)
                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(
                                this,
                                "Incorrect username / password",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                }
            }
            R.id.txKembali -> {
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnLgn.setOnClickListener(this)
        txKembali.setOnClickListener(this)

        val actionBar = supportActionBar
        actionBar!!.title = "Login Admin"

    }


}