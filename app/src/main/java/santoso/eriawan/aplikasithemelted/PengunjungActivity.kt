package santoso.eriawan.aplikasithemelted

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_admin.*
import kotlinx.android.synthetic.main.activity_pengunjung.*

class PengunjungActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "themelted"
    val F_ID = "id"
    val F_PRODUK = "nama"
    val F_HARGA = "harga"
    val F_DESKRIPSI = "deskripsi"
    var docId = ""
    lateinit var db1: FirebaseFirestore
    lateinit var alFile2: ArrayList<HashMap<String, Any>>
    lateinit var adapter: PengunjungAdapter
    lateinit var storage: StorageReference
    lateinit var uri: Uri
    var fbauth = FirebaseAuth.getInstance()

    val F_NAME = "file_name"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    val RC_OK = 100
    var fileType =""
    var fileName =""



    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(p0: View?) {
        TODO("Not yet implemented")
    }

    fun showDataPengunjung() {
        db1.collection(COLLECTION).get().addOnSuccessListener { result ->
            alFile2.clear()
            for (doc in result) {
                val hm = HashMap<String, Any>()
//                hm.set(F_ID, doc.get(F_ID).toString())
                hm.set(F_PRODUK, doc.get(F_PRODUK).toString())
                hm.set(F_HARGA, doc.get(F_HARGA).toString())
                hm.set(F_DESKRIPSI, doc.get(F_DESKRIPSI).toString())
                hm.put(F_NAME, doc.get(F_NAME).toString())
                hm.put(F_TYPE, doc.get(F_TYPE).toString())
                hm.put(F_URL, doc.get(F_URL).toString())
                alFile2.add(hm)
            }
            adapter = PengunjungAdapter(this, alFile2)
            lsProduk.adapter = adapter
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pengunjung)
        alFile2 = ArrayList()
        uri = Uri.EMPTY

        val actionBar = supportActionBar
        actionBar!!.title = "Pricelist Menu"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db1 = FirebaseFirestore.getInstance()
        db1.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("firestore", e.message.toString())
            showDataPengunjung()
        }
    }
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
//            if(data != null) {
//                uri = data.data!!
//                txurl.setText(uri.toString())
//            }
//        }
//    }
}