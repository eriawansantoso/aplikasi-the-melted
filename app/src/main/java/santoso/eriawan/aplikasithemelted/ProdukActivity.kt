package santoso.eriawan.aplikasithemelted

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_admin.*
import kotlinx.android.synthetic.main.activity_pengunjung.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ProdukActivity : AppCompatActivity() , View.OnClickListener {

    val COLLECTION = "themelted"
    val F_ID = "id"
    val F_PRODUK = "nama"
    val F_HARGA = "harga"
    val F_DESKRIPSI = "deskripsi"
    var docId = ""
    lateinit var db1: FirebaseFirestore
    lateinit var alFile: ArrayList<HashMap<String, Any>>
    lateinit var adapter: CustomAdapter
    lateinit var storage: StorageReference
    lateinit var uri: Uri
    var fbauth = FirebaseAuth.getInstance()

    val F_NAME = "file_name"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    val RC_OK = 100
    var fileType =""
    var fileName =""


    override fun onClick(p0: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when (p0?.id) {
            R.id.btnSignUp ->{
                var intent = Intent(this, SignUpActivity::class.java)
                startActivity(intent)
            }
            R.id.btnLogOut ->{
                fbauth.signOut()
                var intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
            R.id.btnUpImg ->{
                fileType = ".jpg"
                intent.setType("image/*")
                startActivityForResult(intent, RC_OK)}
            R.id.btnInput -> {
                if (uri != null) {
                    fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                    val fileRef = storage.child(fileName + fileType)
                    fileRef.putFile(uri)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            return@Continuation fileRef.downloadUrl
                        })
                        .addOnCompleteListener { taks ->
                            val hm = HashMap<String, Any>()
                            hm.set(F_ID, edId.text.toString())
                            hm.set(F_PRODUK, edProduk.text.toString())
                            hm.set(F_HARGA, edHarga.text.toString())
                            hm.set(F_DESKRIPSI, edDesk.text.toString())
                            hm.put(F_NAME, fileName)
                            hm.put(F_TYPE, fileType)
                            hm.put(F_URL, taks.result.toString())


                            db1.collection(COLLECTION).document(edId.text.toString()).set(hm)
                                .addOnSuccessListener {
                                    Toast.makeText(
                                        this,
                                        "Data Successfully added",
                                        Toast.LENGTH_SHORT
                                    )
                                        .show()
                                    edId.setText("")
                                    edProduk.setText("")
                                    edHarga.setText("")
                                    edDesk.setText("")
                                    txurl.setText("")

                                }
                                .addOnFailureListener { e ->
                                    Toast.makeText(
                                        this,
                                        "Data Unsuccessfully added : ${e.message}",
                                        Toast.LENGTH_SHORT
                                    )
                                        .show()
                                    edId.setText("")
                                    edProduk.setText("")
                                    edHarga.setText("")
                                    edDesk.setText("")
                                    txurl.setText("")
                                }
                        }
                }

            }
            R.id.btnDelete -> {
                FirebaseStorage.getInstance().reference.child(fileName+fileType).delete()
//                    .addOnSuccessListener {
                        db1.collection(COLLECTION).whereEqualTo(F_ID, docId).get()
                            .addOnSuccessListener { results ->
                                for (doc in results) {
                                    db1.collection(COLLECTION).document(doc.id).delete()
                                        .addOnSuccessListener {
                                            Toast.makeText(
                                                this,
                                                "Data Successfully Deleted",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                            edId.setText("")
                                            edProduk.setText("")
                                            edHarga.setText("")
                                            edDesk.setText("")
                                            txurl.setText("")
                                        }.addOnFailureListener { e ->
                                            Toast.makeText(
                                                this,
                                                "Data Unsuccessfully Deleted : ${e.message}",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                            edId.setText("")
                                            edProduk.setText("")
                                            edHarga.setText("")
                                            edDesk.setText("")
                                            txurl.setText("")
                                        }
                                }
                            }.addOnFailureListener { e ->
                                Toast.makeText(
                                    this,
                                    "Can't get data's references : ${e.message}",
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                                edId.setText("")
                                edProduk.setText("")
                                edHarga.setText("")
                                edDesk.setText("")
                                txurl.setText("")
                            }
            }


            R.id.btnUpdate -> {
                fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                val fileRef = storage.child(fileName + fileType)
                fileRef.putFile(uri)
                    .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                        return@Continuation fileRef.downloadUrl
                    })
                    .addOnCompleteListener { taks ->
                        val hm = HashMap<String, Any>()
                        hm.set(F_ID, edId.text.toString())
                        hm.set(F_PRODUK, edProduk.text.toString())
                        hm.set(F_HARGA, edHarga.text.toString())
                        hm.set(F_DESKRIPSI, edDesk.text.toString())
                        hm.put(F_NAME, fileName)
                        hm.put(F_TYPE, fileType)
                        hm.put(F_URL, taks.result.toString())


                        db1.collection(COLLECTION).document(edId.text.toString()).update(hm)
                            .addOnSuccessListener {
                                Toast.makeText(
                                    this,
                                    "Data Successfully added",
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                                edId.setText("")
                                edProduk.setText("")
                                edHarga.setText("")
                                edDesk.setText("")
                                txurl.setText("")

                            }
                            .addOnFailureListener { e ->
                                Toast.makeText(
                                    this,
                                    "Data Unsuccessfully added : ${e.message}",
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                                edId.setText("")
                                edProduk.setText("")
                                edHarga.setText("")
                                edDesk.setText("")
                                txurl.setText("")
                            }
                    }
            }
        }
 //       if(p0?.id != R.id.btnInput) startActivityForResult(intent, RC_OK)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)

        val actionBar = supportActionBar
        actionBar!!.title = "Kelola Produk"

        alFile = ArrayList()
        uri = Uri.EMPTY
        btnInput.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnUpImg.setOnClickListener(this)
        btnSignUp.setOnClickListener(this)
        btnLogOut.setOnClickListener(this)
        lsPdkAdmin.setOnItemClickListener(itemClick)

    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db1 = FirebaseFirestore.getInstance()
        db1.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("firestore", e.message.toString())
            showData()
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alFile.get(position)
        docId = hm.get(F_ID).toString()
        edId.setText(docId)
        edProduk.setText(hm.get(F_PRODUK).toString())
        edHarga.setText(hm.get(F_HARGA).toString())
        edDesk.setText(hm.get(F_DESKRIPSI).toString())
        fileType = hm.get(F_TYPE).toString()
        fileName = hm.get(F_NAME).toString()
        txurl.setText(hm.get(F_NAME).toString())
    }

    fun showData() {
            db1.collection(COLLECTION).get().addOnSuccessListener { result ->
                alFile.clear()
                for (doc in result) {
                    val hm = HashMap<String, Any>()
                    hm.set(F_ID, doc.get(F_ID).toString())
                    hm.set(F_PRODUK, doc.get(F_PRODUK).toString())
                    hm.set(F_HARGA, doc.get(F_HARGA).toString())
                    hm.set(F_DESKRIPSI, doc.get(F_DESKRIPSI).toString())
                    hm.put(F_NAME, doc.get(F_NAME).toString())
                    hm.put(F_TYPE, doc.get(F_TYPE).toString())
                    hm.put(F_URL, doc.get(F_URL).toString())
                    alFile.add(hm)
                }
//                adapter = SimpleAdapter(
//                    this, alFile, R.layout.row_admin,
//                    arrayOf(F_ID, F_PRODUK, F_HARGA, F_DESKRIPSI, F_URL),
//                    intArrayOf(R.id.txId1, R.id.txProduk1, R.id.txHarga1, R.id.txDesc1, R.id.imgProduk2)
//                )
                adapter = CustomAdapter(this,alFile)
                lsPdkAdmin.adapter = adapter
            }
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
            if(data != null) {
                uri = data.data!!
                txurl.setText(uri.toString())
            }
        }
    }



}