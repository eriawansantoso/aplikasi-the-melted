package santoso.eriawan.aplikasithemelted

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class PengunjungAdapter(val context: Context,
                        arrayList : ArrayList<HashMap<String, Any>>) : BaseAdapter() {
    val F_ID = "id"
    val F_PRODUK = "nama"
    val F_HARGA = "harga"
    val F_DESKRIPSI = "deskripsi"
    val F_NAME = "file_name"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    val list = arrayList
    var uri = Uri.EMPTY

    inner class ViewHolder() {
        //        var txI: TextView? = null
        var txNamaProduk: TextView? = null
        var txHargaProduk: TextView? = null
        var txDes: TextView? = null
        var txFileName: TextView? = null
        var txFileType: TextView? = null
        var txFileURL: TextView? = null
        var imv2: ImageView? = null
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view: View? = convertView
        if (convertView == null) {
            var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_pengunjung, null, true)

//            holder.txId = view!!.findViewById(R.id.txId1) as TextView
            holder.txNamaProduk = view!!.findViewById(R.id.txNamaProduk) as TextView
            holder.txHargaProduk = view!!.findViewById(R.id.txHargaProduk) as TextView
            holder.txDes = view!!.findViewById(R.id.txDesProduk) as TextView
            holder.imv2 = view!!.findViewById(R.id.imgProduk) as ImageView

            view.tag = holder
        } else {
            holder = view!!.tag as ViewHolder
        }

        var fileType: String = list.get(position).get(F_TYPE).toString()
        uri = Uri.parse(list.get(position).get(F_URL).toString())

//        holder.txId!!.setText(list.get(position).get(F_ID).toString())
        holder.txNamaProduk!!.setText(list.get(position).get(F_PRODUK).toString())
        holder.txHargaProduk!!.setText(list.get(position).get(F_HARGA).toString())
        holder.txDes!!.setText(list.get(position).get(F_DESKRIPSI).toString())



        when (fileType) {
//            ".pdf" -> {holder.imv!!.setImageResource(android.R.drawable.ic_dialog_dialer)}
//            ".docx" -> {holder.imv!!.setImageResource(android.R.drawable.ic_menu_edit)}
//            ".mp4" -> {holder.imv!!.setImageResource(android.R.drawable.ic_media_play)}
            ".jpg" -> {
                Picasso.get().load(uri).into(holder.imv2)
            }
        }

        return view!!
    }
}