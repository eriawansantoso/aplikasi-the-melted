package santoso.eriawan.aplikasithemelted

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnLogin2.setOnClickListener(this)
        btnPengunjung.setOnClickListener(this)
        btnProfil.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.btnLogin2 -> {
                var intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
            R.id.btnPengunjung -> {
                var intent = Intent(this, PengunjungActivity::class.java)
                startActivity(intent)
            }
            R.id.btnProfil -> {
                var intent = Intent(this, ProfilActivity::class.java)
                startActivity(intent)
            }

        }
    }
}