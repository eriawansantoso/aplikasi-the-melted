package santoso.eriawan.aplikasithemelted

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_profil.*
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_profil.*

class ProfilActivity : AppCompatActivity(), View.OnClickListener {
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(p0: View?) {
        when(p0?.id) {
            R.id.btnIG -> {
                var webUri = "https://www.instagram.com/themeltedd/?hl=id"
                var intentInternet = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(webUri)
                ) //filter application than can open website
                startActivity(intentInternet)
            }
            R.id.btnWA -> {
                var webUri = "https://api.whatsapp.com/send?phone=62895366439266"
                var intentInternet = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(webUri)
                ) //filter application than can open website
                startActivity(intentInternet)

            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profil)
        btnWA.setOnClickListener(this)
        btnIG.setOnClickListener(this)

        val actionBar = supportActionBar
        actionBar!!.title = "About Us"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

}