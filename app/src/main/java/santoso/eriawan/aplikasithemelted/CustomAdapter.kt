package santoso.eriawan.aplikasithemelted

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class CustomAdapter(val context: Context,
    arrayList : ArrayList<HashMap<String, Any>>) : BaseAdapter(){
    val F_ID = "id"
    val F_PRODUK = "nama"
    val F_HARGA = "harga"
    val F_DESKRIPSI = "deskripsi"
    val F_NAME = "file_name"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    val list = arrayList
    var uri = Uri.EMPTY

    inner class ViewHolder(){
        var txId : TextView? = null
        var txProduk : TextView? = null
        var txHarga : TextView? = null
        var txDeskripsi : TextView? = null
        var txFileName : TextView? = null
        var txFileType : TextView? = null
        var txFileURL : TextView? = null
        var imv : ImageView? = null
    }
    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return  list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
    var holder = ViewHolder()
        var view:View? = convertView
        if(convertView == null) {
            var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_admin, null, true)

            holder.txId = view!!.findViewById(R.id.txId1) as TextView
            holder.txProduk = view!!.findViewById(R.id.txProduk1) as TextView
            holder.txHarga = view!!.findViewById(R.id.txHarga1) as TextView
            holder.txDeskripsi = view!!.findViewById(R.id.txDesc1) as TextView
            holder.imv = view!!.findViewById(R.id.imgProduk2) as ImageView

            view.tag = holder
        }else{
            holder = view!!.tag as ViewHolder
        }

        var fileType:String = list.get(position).get(F_TYPE).toString()
        uri = Uri.parse(list.get(position).get(F_URL).toString())

        holder.txId!!.setText(list.get(position).get(F_ID).toString())
        holder.txProduk!!.setText(list.get(position).get(F_PRODUK).toString())
        holder.txHarga!!.setText(list.get(position).get(F_HARGA).toString())
        holder.txDeskripsi!!.setText(list.get(position).get(F_DESKRIPSI).toString())



        when(fileType){
//            ".pdf" -> {holder.imv!!.setImageResource(android.R.drawable.ic_dialog_dialer)}
//            ".docx" -> {holder.imv!!.setImageResource(android.R.drawable.ic_menu_edit)}
//            ".mp4" -> {holder.imv!!.setImageResource(android.R.drawable.ic_media_play)}
            ".jpg" -> {Picasso.get().load(uri).into(holder.imv)}
        }

        return view!!
    }
}